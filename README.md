# Example of a broken nested @Embedded relationship

All the files relevant to this example are located in those two files:
- [AppDatabase.kt](app/src/main/java/cz/melkamar/roomnestedpojorelationship/AppDatabase.kt)
- [ExampleInstrumentedTest.kt](app/src/androidTest/java/cz/melkamar/roomnestedpojorelationship/ExampleInstrumentedTest.kt)

## Data model

The data model is plain simple:

`Owner` <--(1:N)-- `Child` --(N:1)--> `ReferencedByChild`

Therefore the only entity having any foreign keys is the `Child` - it has one FK to `Owner` and one FK to `ReferencedByChild`.

## The aim

All I want to do is to read an `Owner` from the database with all its associated `Child` objects and for each `Child` object
also the `ReferencedByChild` object that it references.

So there are POJOs which group the entities as such:

```
ChildWithReferenced
    embedded child
    embedded referencedByChild

OwnerWithEverything
    embedded Owner
    relation to ChildWithReferenced
```

## The problem

With classes set up this way, the project will not even build and throw the following error:
```
error: There is a problem with the query: [SQLITE_ERROR] SQL error or missing database (no such column: refByChildId)

(full error:)
e: /Users/melka/AndroidStudioProjects/RoomNestedPojoRelationship/app/build/tmp/kapt3/stubs/debug/cz/melkamar/roomnestedpojorelationship/OwnerWithEverything.java:12: error: There is a problem with the query: [SQLITE_ERROR] SQL error or missing database (no such column: refByChildId)
    private java.util.List<cz.melkamar.roomnestedpojorelationship.ChildWithReferenced> childrenWithReferenced;
```

Commenting out