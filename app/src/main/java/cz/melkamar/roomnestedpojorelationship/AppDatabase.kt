package cz.melkamar.roomnestedpojorelationship

import android.content.Context
import androidx.room.*

@Database(
    entities = [
        Owner::class,
        Child::class,
        ReferencedByChild::class
    ],
    version = 1
)
abstract class AppDatabase : RoomDatabase() {
    abstract fun appDao(): AppDao

    companion object : SingletonHolder<AppDatabase, Context>({ applicationContext ->
        Room
            .databaseBuilder(applicationContext, AppDatabase::class.java, "example.db")
            .fallbackToDestructiveMigration()
            .build()
    })
}

@Dao
abstract class AppDao {
    @Insert
    abstract fun insertOwner(owner: Owner): Long

    @Insert
    abstract fun insertChild(child: Child): Long

    @Insert
    abstract fun insertReferencedByChild(referencedByChild: ReferencedByChild): Long

    @Query("SELECT * FROM Child INNER JOIN ReferencedByChild ON Child.referencedByChildId = ReferencedByChild.refByChildId ORDER BY Child.childText")
    abstract fun findAllChildrenWithReferencedClasses(): List<ChildWithReferenced>

    // Commenting this query out makes the build pass, so something here is incorrect.
    @Query("SELECT * FROM Owner")
    abstract fun findOwnersWithEverything(): List<OwnerWithEverything>
}

// ENTITIES
@Entity
data class Owner(
    @PrimaryKey(autoGenerate = true)
    val ownerId: Long,
    val ownerText: String
)

@Entity(
    foreignKeys = [
        ForeignKey(
            entity = Owner::class,
            parentColumns = arrayOf("ownerId"),
            childColumns = arrayOf("referencedOwnerId"),
            onDelete = ForeignKey.CASCADE
        ),
        ForeignKey(
            entity = ReferencedByChild::class,
            parentColumns = arrayOf("refByChildId"),
            childColumns = arrayOf("referencedByChildId"),
            onDelete = ForeignKey.CASCADE
        )
    ]
)
data class Child(
    @PrimaryKey(autoGenerate = true)
    val childId: Long,
    val childText: String,
    val referencedOwnerId: Long,
    val referencedByChildId: Long
)

@Entity
data class ReferencedByChild(
    @PrimaryKey(autoGenerate = true)
    val refByChildId: Long,
    val refText: String
)

// POJOS

// The Child has exactly one ReferencedByChild reference. This POJO joins those two
class ChildWithReferenced(
    @Embedded
    var child: Child,

    @Embedded
    var referencedByChild: ReferencedByChild
)

class OwnerWithEverything {
    @Embedded
    var owner: Owner? = null

    @Relation(
        parentColumn = "ownerId",
        entityColumn = "referencedOwnerId",
        entity = Child::class  // which entity should be defined here?
    )
    var childrenWithReferenced: List<ChildWithReferenced>? = null
}

// HELPER CLASSES
open class SingletonHolder<T, A>(creator: (A) -> T) {
    private var creator: ((A) -> T)? = creator
    @Volatile
    private var instance: T? = null

    fun getInstance(arg: A): T {
        val i = instance
        if (i != null) {
            return i
        }

        return synchronized(this) {
            val i2 = instance
            if (i2 != null) {
                i2
            } else {
                val created = creator!!(arg)
                instance = created
                creator = null
                created
            }
        }
    }
}