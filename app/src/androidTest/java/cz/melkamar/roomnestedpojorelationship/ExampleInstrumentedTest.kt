package cz.melkamar.roomnestedpojorelationship

import android.content.Context
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import java.io.InvalidObjectException

/**
 * Instrumented test, which will execute on an Android device.
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(AndroidJUnit4::class)
class ExampleInstrumentedTest {
    private lateinit var dao: AppDao
    private lateinit var db: AppDatabase

    @Before
    fun createDb() {
        val context = ApplicationProvider.getApplicationContext<Context>()
        db = Room.inMemoryDatabaseBuilder(
            context, AppDatabase::class.java
        ).build()
        dao = db.appDao()
    }

    @Test
    fun testSaveAndLoadChildWithReferenced(){
        var ownerId = dao.insertOwner(Owner(0, "owner"))

        var refId = dao.insertReferencedByChild(ReferencedByChild(0, "first referencedByChild"))
        dao.insertChild(Child(0, "first child", ownerId, refId))

        refId = dao.insertReferencedByChild(ReferencedByChild(0, "second referencedByChild"))
        dao.insertChild(Child(0, "second child", ownerId, refId))

        refId = dao.insertReferencedByChild(ReferencedByChild(0, "third referencedByChild"))
        dao.insertChild(Child(0, "third child", ownerId, refId))

        val childrenWithRefs = dao.findAllChildrenWithReferencedClasses()

        Assert.assertEquals(3, childrenWithRefs.size)

        Assert.assertEquals("first child", childrenWithRefs[0].child.childText)
        Assert.assertEquals("first referencedByChild", childrenWithRefs[0].referencedByChild.refText)

        Assert.assertEquals("second child", childrenWithRefs[1].child.childText)
        Assert.assertEquals("second referencedByChild", childrenWithRefs[1].referencedByChild.refText)

        Assert.assertEquals("third child", childrenWithRefs[2].child.childText)
        Assert.assertEquals("third referencedByChild", childrenWithRefs[2].referencedByChild.refText)

    }

    fun testSaveAndLoadOwnerWithEverything() {
        var ownerId = dao.insertOwner(Owner(0, "owner"))

        var refId = dao.insertReferencedByChild(ReferencedByChild(0, "first referencedByChild"))
        dao.insertChild(Child(0, "first child", ownerId, refId))

        refId = dao.insertReferencedByChild(ReferencedByChild(0, "second referencedByChild"))
        dao.insertChild(Child(0, "second child", ownerId, refId))

        refId = dao.insertReferencedByChild(ReferencedByChild(0, "third referencedByChild"))
        dao.insertChild(Child(0, "third child", ownerId, refId))

        val owners = dao.findOwnersWithEverything()

        Assert.assertEquals(1, owners.size)
        val owner = owners[0]

        val childrenWithReferenced = owner.childrenWithReferenced?:throw InvalidObjectException("List is null")
        Assert.assertEquals("first child", childrenWithReferenced[0].child.childText)
        Assert.assertEquals("first referencedByChild", childrenWithReferenced[0].referencedByChild.refText)

        Assert.assertEquals("second child", childrenWithReferenced[1].child.childText)
        Assert.assertEquals("second referencedByChild", childrenWithReferenced[1].referencedByChild.refText)

        Assert.assertEquals("third child", childrenWithReferenced[2].child.childText)
        Assert.assertEquals("third referencedByChild", childrenWithReferenced[2].referencedByChild.refText)
    }
}
